---
title: "Portfolio"
description: "This is a list of [free and open-source] projects I am working on, or am a part of. You will also find projects I have worked on in the past."
layout: general
order: 2
---

{% include command.html command="open portfolio.html" %}

{{ page.description }}

Legend:
- 👤: Personal project
- 🫂: In collaboration with other developers
- 🏫: Course project

## Coding

### [theevilskeleton.gitlab.io] 👤
Personal website developed and designed from scratch, powered by [Jekyll] and GitLab Pages.

### [Bottles] 🫂
Safely run Windows software on Linux. Written in Python using PyGObject.

{% include image.html url="/assets/bottles.webp" %}

### [Upscaler] 🫂 🏫
Upscale and enhance images.

{% include image.html url="/assets/upscaler.webp" %}

### [flatpak-dedup-checker] 👤
Analyze the amount of data used in a Flatpak directory.

```
Usage: flatpak-dedup-checker [options] COMMAND

-h, --help              Print the help section and exit
-p PATH, --path=PATH    Specify custom path (default: /var/lib/flatpak)
-r, --runtime           List data only used by runtimes
-a, --app               List data only used by applications
--user                  Work on the user installation
```

### [overlay] 👤 🏫
A command-line utility and Python library for reading and writing vkBasalt and MangoHud configurations on Linux.

```
usage: project.py [-h] [--check-layers] {vkbasalt,mangohud} ...

parse and write configurations for vkBasalt and MangoHud

options:
  -h, --help           show this help message and exit
  --check-layers       check if vkBasalt, MangoHud and Mangoapp are available

layers:
  layer specific options

  {vkbasalt,mangohud}
    vkbasalt           use vkbasalt
    mangohud           use mangohud
```

### [vkbasalt-cli] 👤
A command-line utility and Python library for reading and writing vkBasalt configurations on Linux. (Superseded by [overlay].)

```
optional arguments:
  -h, --help            show this help message and exit
  -e {cas,dls,fxaa,smaa,lut} [{cas,dls,fxaa,smaa,lut} ...], --effects {cas,dls,fxaa,smaa,lut} [{cas,dls,fxaa,smaa,lut} ...]
                        effects in a separated list of effect to use
  -o OUTPUT, --output OUTPUT
                        output file
  -d, --default         use default configuration
  --toggle-key TOGGLE_KEY
                        toggle key (default: Home)
  --disable-on-launch   disable on launch
  --cas-sharpness CAS_SHARPNESS
                        adjust CAS sharpness
  --dls-sharpness DLS_SHARPNESS
                        adjust DLS sharpness
  --dls-denoise DLS_DENOISE
                        adjust DLS denoise
  --fxaa-subpixel-quality FXAA_SUBPIXEL_QUALITY
                        adjust FXAA subpixel quality
  --fxaa-quality-edge-threshold FXAA_QUALITY_EDGE_THRESHOLD
                        adjust FXAA quality edge threshold
  --fxaa-quality-edge-threshold-min FXAA_QUALITY_EDGE_THRESHOLD_MIN
                        adjust FXAA quality edge threshold minimum
  --smaa-edge-detection {luma,color}
                        adjust SMAA edge detection (default: luma)
  --smaa-threshold SMAA_THRESHOLD
                        adjust SMAA threshold
  --smaa-max-search-steps SMAA_MAX_SEARCH_STEPS
                        adjust SMAA max search steps
  --smaa-max-search-steps-diagonal SMAA_MAX_SEARCH_STEPS_DIAGONAL
                        adjust SMAA max search steps diagonal
  --smaa-corner-rounding SMAA_CORNER_ROUNDING
                        adjust SMAA corner rounding
  --lut-file-path LUT_FILE_PATH
                        specify LUT file path
  --exec EXEC           execute command
```

### [Finance] 🏫

Implemented a website that allows users to buy and sell stocks using the IEX Cloud API. Written in Python and JavaScript using Flask and Bootstrap.

{% include image.html url="/assets/finance.webp" %}

### [Spell Checker] 🏫

Implemented a program that spell-checks files. Written in C, using a hash table.

```
$ ./speller texts/lalaland.txt

MISSPELLED WORDS
[…]

WORDS MISSPELLED:     955
WORDS IN DICTIONARY:  143091
WORDS IN TEXT:        17756
TIME IN load:         0.04
TIME IN check:        0.03
TIME IN size:         0.00
TIME IN unload:       0.01
TIME IN TOTAL:        0.08
```

### [Image Recovery] 🏫

Implemented a program that recovers JPEGs from a forensic image. Written in C.

---

## Technical Writing

### [Flatseal]

Created and improved the [documentation](https://github.com/tchx84/Flatseal/blob/master/DOCUMENTATION.md).

### [Flatpak]

Improved the documentation for developers.

### [Fedora Websites]

Documented the contributing guidelines.

---

## Organizations/Communities

### [GNOME Foundation]

A nonprofit organization that funds and coordinates the GNOME desktop environment.

I'm a [member of the GNOME Foundation](https://foundation.gnome.org/membership), as I actively contribute to [GNOME software](https://gitlab.gnome.org/TheEvilSkeleton). I also write articles about GNOME on my blog.

{% include image.html url="/assets/gnome.webp" %}

### [Vanilla OS]
A Linux distribution targeting the average computer and mobile users and developers.

I am a [member of Vanilla OS](https://vanillaos.org/team) and responsible for much of the user interface and experience. I contribute code whenever possible and moderate the official Discord server.

{% include image.html url="/assets/vanilla-os.webp" %}

### [The Fedora Project]

A Linux distribution focusing on [free and open-source] software and convenience.

I'm a [member of the editorial team](https://docs.fedoraproject.org/en-US/fedora-magazine/editorial-meetings/) at Fedora Magazine. I also participated in the recent redesign of the Fedora website named [fedora-websites-3.0](https://gitlab.com/fedora/websites-apps/fedora-websites/fedora-websites-3.0).

---

## Miscellaneous

### [Consultant at Flathub]

Packaged and maintained a dozen of apps into [Flatpak](https://flatpak.org) and published them on [Flathub](https://flathub.org).

[free and open-source]: https://en.wikipedia.org/wiki/Free_and_open-source_software
[Bottles]: https://usebottles.com
[Upscaler]: /upscaler
[vkbasalt-cli]: https://gitlab.com/TheEvilSkeleton/vkbasalt-cli
[vkBasalt]: https://github.com/DadSchoorse/vkBasalt
[flatpak-dedup-checker]: https://gitlab.com/TheEvilSkeleton/flatpak-dedup-checker
[src_prepare]: https://src_prepare.gitlab.io
[vkbasalt-cli]: https://gitlab.com/TheEvilSkeleton/vkbasalt-cli
[overlay]: https://gitlab.com/TheEvilSkeleton/overlay
[GNOME Foundation]: https://foundation.gnome.org
[Vanilla OS]: https://vanillaos.org
[The Fedora Project]: https://fedoraproject.org
[Finance]: https://cs50.harvard.edu/x/2022/psets/9/finance
[IEX Cloud]: https://iexcloud.io/docs
[theevilskeleton.gitlab.io]: https://gitlab.com/TheEvilSkeleton/theevilskeleton.gitlab.io
[Jekyll]: https://jekyllrb.com
[Flatseal]: https://github.com/tchx84/Flatseal/pulls?q=is%3Apr+author%3ATheEvilSkeleton+
[Flatpak]: https://github.com/flatpak/flatpak-docs/pulls?q=is%3Apr+author%3ATheEvilSkeleton+
[Fedora Websites]: https://gitlab.com/groups/fedora/websites-apps/fedora-websites/-/wikis/Contributor-Guidelines
[Consultant at Flathub]: https://flathub.org/consultants
[Spell Checker]: https://cs50.harvard.edu/x/2022/psets/5/speller
[Image Recovery]: https://cs50.harvard.edu/x/2022/psets/4/recover
